    

    const playerBatu = document.getElementById("playerBatu")
    const playerGunting = document.getElementById("playerGunting")
    const playerKertas = document.getElementById("playerKertas")
    const memilih = document.getElementsByClassName("hasilSuit")
    const keterangan = document.getElementById("keterangan")
    const compBatu = document.getElementById("comBatu")
    const compKertas = document.getElementById("comKertas")
    const compGunting = document.getElementById("comGunting")
    const refresh = document.getElementById("refresh")
    refresh.style.display = "none"


    // THIS IS NOT THE BEST WAY.. BUT I WAS STUCK AND DIDN'T HAVE TIME
    // PLAYER CLICK BATU
    let choice = ["comBatu", "comGunting", "comKertas"]
    let compChoice = null
    const element1 = document.getElementById("playerBatu");
    element1.addEventListener("click", playerClickBatu);
    function playerClickBatu() {
        let player = "playerBatu" 
        getCompChoice()
        if (compChoice === "comBatu") {
            hideIcons()
            youDraw()
            compBatu.style.display = "block"
            playerBatu.style.display = "block"

        } else if (compChoice === "comKertas"){
            youLose()
            hideIcons()
            compKertas.style.display = "block"
            playerBatu.style.display = "block"

        }else{
            youWin()
            hideIcons()
            compGunting.style.display = "block"
            playerBatu.style.display = "block"
        }
    }

    // PLAYER CLICK KERTAS
    const element2 = document.getElementById("playerKertas");
    element2.addEventListener("click", playerClickKertas);
    function playerClickKertas() {
        let player = "playerKertas" 
        getCompChoice()
        if (compChoice === "comKertas") {
            hideIcons()
            youDraw()
            compKertas.style.display = "block"
            playerKertas.style.display = "block"

        } else if (compChoice === "comGunting"){
            youLose()
            hideIcons()
            compGunting.style.display = "block"
            playerKertas.style.display = "block"

        }else{
            youWin()
            hideIcons()
            compBatu.style.display = "block"
            playerKertas.style.display = "block"
        }
    }

    // PLAYER CLICK GUNTING
    const element3 = document.getElementById("playerGunting");
    element3.addEventListener("click", playerClickGunting);
    function playerClickGunting() {
        let player = "playerGunting" 
        getCompChoice()
        if (compChoice === "comGunting") {
            hideIcons()
            youDraw()
            compGunting.style.display = "block"
            playerGunting.style.display = "block"

        } else if (compChoice === "comBatu"){
            youLose()
            hideIcons()
            compBatu.style.display = "block"
            playerGunting.style.display = "block"

        }else{
            youWin()
            hideIcons()
            compKertas.style.display = "block"
            playerGunting.style.display = "block"

        }
    }


    function getCompChoice() {
        compChoice = choice[Math.floor(Math.random() * choice.length)]
        showRefresh()
    }

    function youWin() {
        keterangan.innerHTML = "YOU WIN"
        keterangan.style.background = "green"
    }
    function youLose() {
        keterangan.innerHTML = "YOU LOSE"
        keterangan.style.background = "red"
    }
    function youDraw() {
        keterangan.innerHTML = "DRAW"
        keterangan.style.background = "white"
    }
    function hideIcons() {
        playerKertas.style.display = "none"
        playerGunting.style.display = "none"
        playerBatu.style.display = "none"  
        compKertas.style.display = "none"
        compBatu.style.display = "none"
        compGunting.style.display = "none"
        // refresh.style.display = "none"
    };

    function showRefresh() {
        refresh.style.display = "block"
        refresh.addEventListener("click", toRefresh)
        function toRefresh() { 
            window.location.reload(true)
        }
    }   
